O:  
1.早上参与了代码审查（Code Review）活动，由Natalie进行指导。她指出了我的测试方法命名不正确、缺少原子操作意识以及if判断书写错误的问题。  
2.上午进行了团队的Session Report，我们的主题是策略模式。我负责进行"What kind of problems does it solve"的讲解。通过演讲和听其他组同学们的Report。  
3.下午我们进行了代码重构（Code Refactor）的学习与训练。学习了代码味道（Code Smell）的内容，其中重点包括Mysterious Name、Duplicated Code、Feature Envy、Loops等。  
  
R：\
充实、有点艰难（Refactor时）\
\
I：\
1.Code Review这让我对代码的规范性编写有了更深入的了解。\
2.Session Report让我初步了解了设计模式的应用场景和解决问题的能力。\
3.Code Refacfor让我对如何改进这些代码异味有了更清晰的认识,但是在进行Refactor实操时我感到有些艰难，不知道怎么用stream去转化loops。\
\
D：\
我会主动寻找Stream相关的教程和示例代码，并进行练习和实践，以便更熟练地运用Stream来优化代码逻辑。如果遇到问题，我也会向同事或专门的资源寻求帮助。
