import java.util.*;
import java.util.stream.Collectors;

public class WordFrequencyGame {
    public static final String ERROR_MESSAGE = "Calculate Error";
    public static final String REGEX = "\\s+";

    public String getResult(String inputStr) {
        try {
            List<WordFrequency> wordFrequencies = getWordFrequencyList(inputStr);
            Map<String, List<WordFrequency>> wordFrequencyGroupByWord = groupFrequencyByWord(wordFrequencies);
            List<WordFrequency> wordFrequenciesWithCount = getWordFrequencyListWithCount(wordFrequencyGroupByWord);
            List<WordFrequency> sortedWordFrequencies = sortWordFrequenciedList(wordFrequenciesWithCount);
            return formatResult(sortedWordFrequencies);
        } catch (Exception e) {
            return ERROR_MESSAGE;
        }
    }

    private static String formatResult(List<WordFrequency> wordFrequenciesWithCount) {
        return wordFrequenciesWithCount.stream()
                .map(wordFrequency -> wordFrequency.getValue() + " " + wordFrequency.getWordCount())
                .collect(Collectors.joining("\n"));
    }

    private static List<WordFrequency> sortWordFrequenciedList(List<WordFrequency> wordFrequenciesWithCount) {
        wordFrequenciesWithCount.sort((firstWord, secondWord) -> secondWord.getWordCount() - firstWord.getWordCount());
        return wordFrequenciesWithCount;
    }

    private static List<WordFrequency> getWordFrequencyListWithCount(Map<String, List<WordFrequency>> wordFrequencyMap) {
        return wordFrequencyMap.keySet().stream().map(key -> new WordFrequency(key, wordFrequencyMap.get(key).size())).collect(Collectors.toList());
    }

    private static List<WordFrequency> getWordFrequencyList(String inputStr) {
        return Arrays.stream(inputStr.split(REGEX)).map(word -> new WordFrequency(word, 1)).collect(Collectors.toList());
    }

    private Map<String, List<WordFrequency>> groupFrequencyByWord(List<WordFrequency> wordFrequencyList) {
        Map<String, List<WordFrequency>> wordFrequencyMap = new HashMap<>();
        wordFrequencyList.forEach(input -> wordFrequencyMap.computeIfAbsent(input.getValue(), k -> new ArrayList<>()).add(input));
        return wordFrequencyMap;
    }
}